#!/usr/bin/env python
import imaplib,getpass,argparse,os,datetime,re

def findtimestamp(message):
    # Find index of second iteration of "Received" string
    times = re.findall(r'(\d+:\d+:\d+)',message)
    for n in [1,0,2]:
        try:
            time = times[n]
            # Get position of first iteration of time string
            idate = message.find(time)
            # Extract full date and time
            date = re.split(' |:',message[idate-12:idate+8].strip())
            # Convert to timestamp
            timestamp = datetime.datetime(int(date[2]),months[date[1]],int(date[0]),
                                          int(date[3]),int(date[4]),int(date[5]))
            break
        except:
            continue
    return timestamp.strftime('%y%m%d_%H%M%S')

if __name__ == '__main__':

    # Extract arguments
    argparser = argparse.ArgumentParser(description="Dump a IMAP folder into .eml files")
    argparser.add_argument('-s', dest='host', help="IMAP host, like imap.gmail.com")
    argparser.add_argument('-p', dest='password', help="IMAP password")
    argparser.add_argument('-u', dest='username', help="IMAP username")
    argparser.add_argument('-l', dest='local_folder', help="Local folder where to save .eml files", default='.')
    argparser.add_argument('-r', dest='remote_folder', help="Remote folder to download", default='INBOX')
    argparser.add_argument('-i','--index', type=int, default=1, help="Minimum index")
    argparser.add_argument('-d','--dirlist', action='store_true', help="List remote folders on server")
    argparser.add_argument('-c','--count', action='store_true', help="Only return number of messages found")
    args = argparser.parse_args()
    months = {'Jan':1,'Feb':2,'Mar':3,'Apr':4,'May':5,'Jun':6,'Jul':7,'Aug':8,'Sep':9,'Oct':10,'Nov':11,'Dec':12}
    # Connecting to server
    print('Connecting to %s...'%args.host)
    server = imaplib.IMAP4_SSL(args.host)
    # Logging in
    print('Login to %s...'%args.username)
    pwd = args.password if args.password!=None else getpass.getpass()
    server.login(args.username,pwd)
    # List of remote folders if requested
    if args.dirlist:
        for i in server.list()[1]:
            print(i)
        quit()
    # Select remote folder on server
    server.select(args.remote_folder,readonly=True)
    # Search all messages in remote folder
    typ, data = server.search(None, 'ALL')
    mail_idxs = data[0].split()
    print('%i messages found in %s remote folder'%(len(mail_idxs),args.remote_folder))
    if args.count: quit()
    for num in range(args.index,len(mail_idxs)+1):
        print('Saving message %04i ...'%num)
        attempts = 0
        while attempts < 6:
            try:
                print('Fetching...')
                # Fetching message from server
                typ, msg_data = server.fetch(str(num), '(RFC822)')
                timestamp = findtimestamp(str(msg_data[0][1]))
                # Define output filename
                new_path = '%s/%s_%04i.eml'%(args.local_folder,timestamp,int(num))
                f = open(new_path, 'wb')
                f.write(msg_data[0][1])
                f.close()
                print('Saved as',new_path)
                break
            except imaplib.IMAP4.abort:
                attempts += 1
                print('Failed! Trying again...')
                server = imaplib.IMAP4_SSL(args.host)
                server.login(args.username,pwd)
                server.select(args.remote_folder,readonly=True)
        if attempts==3:
            print('3 failed attempts. Abort!')
            quit()
    server.close()
    server.logout()
    
