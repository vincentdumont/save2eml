# Save remote emails to local .eml files

Inspired by [this script](https://gist.github.com/robulouski/7442321), the routine in this repository allows to access an email server via IMAP and download all the emails from a particular remote folder. Here's an examle of how it works: 

```
./readIMAP.py -s imap.domain.com -u username@domain.com -r INBOX/Archives/2010 -l 2010
```

In the above example, the program will (1) access via IMAP the domain.com mail server, (2) login to the username account, (3) search for the INBOX/Archives/2010 folder in that account, and (4) download all the emails found in the remote folder and save them in a local folder named 2010.
