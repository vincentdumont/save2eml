#!/usr/bin/env python
import argparse,os,difflib
from readIMAP import findtimestamp

# Extract arguments
argparser = argparse.ArgumentParser(description="Clean .eml files in local repository")
argparser.add_argument('folder',help="Local repository")
argparser.add_argument('action',default='size',help="Rename, check size, check content",choices=['rename','size','content'])
argparser.add_argument('-t',dest='threshold',default=0.00001,type=float,help="Size difference threshold")
argparser.add_argument('-d','--delete', action='store_true', help="Delete files")
args = argparser.parse_args()

# Sorted list of found .eml files in repository
flist = sorted(os.listdir(args.folder))
flist = [fname for fname in flist if '.eml' in fname]

# Initialize list of folder to by deleted
n,to_delete=1,[]

# Scan over all found messages
print('Scanning all %i messages...'%len(flist))
for i,fname in enumerate(flist):
    # Rename files
    if args.action=='rename':
        old_path = args.folder+'/'+emlfile
        try:
            with open(old_path,mode='rb') as file:
                timestamp = findtimestamp(str(file.read()))
            new_path = '%s/%s_%04i.eml'%(args.folder,timestamp,i+1)
            os.rename(old_path,new_path)
        except:
            print('An issue occurred with %s'%old_path)
    # Check file similarity by size
    if i<len(flist)-1 and args.action=='size':
        # Extract size in kilobyte of current and next file
        statinfo1 = os.path.getsize(args.folder+'/'+flist[i])/1024.
        statinfo2 = os.path.getsize(args.folder+'/'+flist[i+1])/1024.
        # Check if size difference is less than 1 kilobyte
        diff = abs(statinfo1-statinfo2)
        if diff<args.threshold:
            print('%04i : %s/%s %s/%s : %f KB'%(n,args.folder,flist[i],args.folder,flist[i+1],diff))
            to_delete.append(args.folder+'/'+flist[i+1])
            n+=1
    # Check file similarity by content
    if i<len(flist)-1 and args.action=='content':
        file1 = open(folder+'/'+flist[i],errors='ignore')
        file2 = open(folder+'/'+flist[i+1],errors='ignore')
        diff = difflib.ndiff(file1.readlines(), file2.readlines())
        delta = ''.join(x[2:] for x in diff if x.startswith('- '))
        if 'Subject' not in delta:
            print('%04i : %s/%s %/s%s'%(n,args.folder,flist[i],args.folder,flist[i+1]))
            to_delete.append(args.folder+'/'+flist[i+1])
            n+=1
if args.delete:
    for fname in to_delete:
        os.remove(fname) 
